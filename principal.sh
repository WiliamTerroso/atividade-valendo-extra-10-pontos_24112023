#!/bin/bash

# Importa as funções do arquivo func.sh
source func.sh

# Solicita o nome do arquivo
echo "Digite o nome do arquivo:"
read arquivo

# Solicita o tipo de exibição
echo "Escolha o tipo de exibição:"
echo "1 - Linhas"
echo "2 - Colunas"
read tipo

# Exibe o arquivo
if [ "$tipo" -eq 1 ]; then
  echo "Exibindo o arquivo linha por linha:"
  linha $arquivo
elif [ "$tipo" -eq 2 ]; then
  echo "Exibindo o arquivo coluna por coluna:"
  for i in $(seq 1 7); do
    coluna $arquivo $i
  done
else
  echo "Opção inválida"
fi

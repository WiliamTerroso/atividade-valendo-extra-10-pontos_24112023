#!/bin/bash

# Função linha
function linha() {
  local arquivo=$1
  local linha=$2

  if [ ! -f "$arquivo" ]; then
    echo "Arquivo $arquivo não encontrado"
    exit 1
  fi

  if [ "$linha" -gt $(wc -l "$arquivo") ]; then
    echo "Linha $linha não existe"
    exit 1
  fi

  cat "$arquivo" | sed -n "$linha"p
}

# Função coluna
function coluna() {
  local arquivo=$1
  local coluna=$2

  if [ ! -f "$arquivo" ]; then
    echo "Arquivo $arquivo não encontrado"
    exit 1
  fi

  if [ "$coluna" -gt 7 ]; then
    echo "Coluna $coluna não existe"
    exit 1
  fi

  cat "$arquivo" | cut -d '' -f 3
}
